**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. Learning plan for the first month: Study Java, React, Agile Development, etc. for the first three weeks, and use Agile Development as a group to complete projects in the fourth week
2. Ice breaking activity: Through ice breaking activities, I gained a better understanding of my colleagues in the class
3. Learning expectations: Everyone has different expectations for training, and the teacher helps us better understand the expected learning outcomes after one month by answering our expectations
4. Build a school in the cloud: The video played by the teacher explains three ways to improve learning efficiency: creating an autonomous learning environment, learning with questions, and stimulating our curiosity and interest
5. Learning Pyramid: People's retention rate of learning content in different ways is mainly divided into passive learning and active learning. The retention rate of active learning is much higher than that of passive learning, so we need to master knowledge more through active learning
6. PDCA, abbreviated as plan, do, check, and adjust, is a work step of comprehensive quality management and a method that we need to apply in the development process
7. Concept map: It mainly helps us form a knowledge system by connecting old things. The constituent elements include event/object, linking words, proposals, and focus question. By drawing a concept map, we can obtain visual thinking, knowledge connection, and summary
8. How to build concept map: Determine Focus Question, Identify key, Rank order concepts, connect concepts, modify structure, add example
9. ORID: Abbreviation for objective, reflective, interpretive, interpretive, and decision, mainly used for personal review through these four dimensions

**R (Reflective): Please use one word to express your feelings about today's class.**

novel

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

I think the learning method of learning while practicing is very good, such as the concept map practice in the afternoon. Our group brainstorming was very chaotic at the beginning, and under the guidance of the teacher, we were able to fully learn how to establish a concept map. At the same time, everyone in the group was a bit hesitant to speak at the beginning, and gradually everyone was able to speak freely. This was the most meaningful training for us today.

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

In future learning, we can master knowledge through the learning pyramid with high retention rate; Develop high-quality code using the work steps of PDCA comprehensive quality management in development; Using concept maps to establish our knowledge system; Use ORID for personal review to help remember daily work tasks and knowledge.